# Functional Programming Fear

## The Protest

Of all protests heard about FP, the most frequently used is the code without
side effects can't _do_ anything. Which is perfectly true, but not the the
point.

The point to to arrange code into modules with perfectly (as perfectly as
possible on the layer that you're writing on); modulew which:

* Have a unary interface
* Do not have side effects (are pure)
* Have expected returned valued.
* In an eager environment, return as expected, and do not call code on import
  (excepting _maybe_ enums or constants).

# A/B Where Versions Mean Nothing

## The Philosophy

There exists a strain of thought, endemic throughout end-user application, that techniques such as focus group testing, A/B tests, statistical regression analysis, and related disciplines are the only path forward for creating a strong human-software UI link. This is wrong.

It should go without saying that it is wrong, but as I'm saying that it, it's clear that it must be said. This is our craft and discipline; our _skill_.

### "Good" Techniques

#### Accessibility, Invisible Markup, DOM-Native Interface

In many circumstances, UI implemtentors have been encouraged to "save bits" on the wire (dubious) by pulling content via frequently non-HTTP request. Examples:
* The "infinite" list. This pulls content via the HTTP client and inserts it into the DOM.

##### Downsides and implmentator errors

"Find" affordances, if not properly implemented, will be broken (you can't search for what's not there if you're depending on searching the DOM; rule #1: don't do it if you don't have to.)
