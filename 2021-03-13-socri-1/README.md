# Why

This is not the first step in a rhetorical regime, nor a logical inquiry.

It _is_ a practical question. In that, it asks why we are pursuing this thread
at all.

## Subjects

### Intersection: Tech and Social
* Etiquette from another planet
* Why's it bad, then?
  * (What about their jobs?)
* Has the pathological need for companionship always been with our species?
* Why functional programming is good, and why TIOBE and other popularity
  contests are not valid measurments of value.
* Why they time has already passed for Atlassian, Monday.com, etc. (before it
  had begun, in most cases.)
* Why VC is terrible at picking winners, and why it doesn't matter at all.
* Ignorance is not a virtue in this case: Why "tech journalism" is awful.

### Pure Tech
* Shell Games: RISC-V
* Seeing the con: Real solution has been around for five years and it must hurt
  to be Clifford Wolf.
* Apple needs to go away for us to proceed.
* Satellite constellations, ballons, tubes, paying for actual engineers: Elon
  Musk is a con man.

### Politics and Liars
* Why people lie in exit polls (exclusively saying they voted for the winner.)
