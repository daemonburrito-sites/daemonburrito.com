# We Are Not Constrained By Process

This statement is absurd.

We are constrained by ineptitude, lack of continuing education, and curiosity.

"We live in a world where we feel we must learn everything and it's drivng us insane." This is similarly stupid. It's obvious that we should know as much as we can.

## Practitioners

There is a similar class of creators who can be termed "practictioners." They gain this title without any considersation to accreditation, only professional accomplishment.

We kill many of them, much in the same way we killed Ramanujan and Turning by removing them from their enviroment; taking a fish from water.

Taking a life-long devout Hindi vegan and dropping him in the sub-arctic climate of the British Islands, and taking a homosexual at a time when the pendulum has swung towards execution or medicalization (frequently the same) and giving him the choice of humiliation and robbery of identity or death.

## What is the Problem?

We can agree that Zuckerberg was a mediocre PHP hacker who got lucky, and the "constraint of process" is worse than self-help. So where is the the problem?

The problem is the lack of the allowance of imaginination, of economony, and simplicity. Of talent we have no shortage. Of management we have an excess.
